$(document).ready(function () {
    console.log('привет');

    if ($().slick) {
        var option = {
            speed: 500,
            autoplaySpeed: 6000,
            autoplay: true,
            fade: true,
            touchMove: false,
            touchThreshold: 100,
            adaptiveHeight: false,
            prevArrow: '<i class="slick-arrow slick-prev trans"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 21" enable-background="new 0 0 12 21"><path d="M0 10.5c0-.2.1-.4.2-.5l9.6-9.8c.2-.3.6-.3.9 0l1 1.1c.3.3.3.7 0 .9l-8.1 8.3 8.1 8.3c.1.1.2.3.2.5s-.1.4-.2.5l-1 1.1c-.2.3-.6.3-.9 0l-9.6-9.9c-.1-.1-.2-.3-.2-.5z"/></svg></i>',
            nextArrow: '<i class="slick-arrow slick-next trans"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 12 21" enable-background="new 0 0 12 21"><path d="M12 10.5c0-.2-.1-.4-.2-.5l-9.6-9.8c-.2-.3-.6-.3-.9 0l-1 1.1c-.3.3-.3.7 0 .9l8.1 8.3-8.1 8.3c-.2.1-.3.3-.3.5s.1.4.2.5l1 1.1c.2.3.6.3.9 0l9.6-9.8c.2-.2.3-.4.3-.6z"/></svg></i>'
        };

        $('.js-slider').slick(option);
    }
})